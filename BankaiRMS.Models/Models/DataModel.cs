﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BankaiRMS.Models.Models
{
    public class AccHeadsConstants
    {
        //Current Assets
        public static readonly int CASH = 10000;
        public static readonly int ACCOUNT_RECEIVABLE = 12000;
        public static readonly int INVENTORY = 13000;
        public static readonly int SUPPLIES = 14000;
        public static readonly int FIXED_ASSETS = 17000;
        public static readonly int CONTRA_ACCOUNTS = 18000;
        public static readonly int OTHER_ASSETS = 19000;

        //Current Liabilities
        public static readonly int NOTES_PAYABLE = 20000;
        public static readonly int ACCOUNTS_PAYABLE = 21000;
        public static readonly int WAGES_PAYABLE = 22000;
        public static readonly int INTEREST_PAYABLE = 23000;
        public static readonly int UNEARNED_REVENUES = 24000;
        public static readonly int MORTGAGE_LOAN_PAYABLE = 22000;

        //Equity
        public static readonly int CREDIT_LINE = 30000;
        public static readonly int CASHED_IN_EQUITY = 31000;

        //Income
        public static readonly int SALES = 40000;

        //Expense
        public static readonly int COGS = 50000;

        //Gains/Losses
        public static readonly int NON_OPERATING_INCOME = 60000;
    }

    public class AccountConstants
    {
        //Accounts for Current Assets
        public static readonly int CHECKING = 10100;
        public static readonly int SAVING = 10200;
        public static readonly int ACCOUNT_RECEIVABLE = 12100;
        public static readonly int INVENTORY = 13100;
        public static readonly int SUPPLIES = 14100;
        public static readonly int LAND = 17100;
        public static readonly int BUILDING = 17200;
        public static readonly int EQUIPMENT = 17300;
        public static readonly int VEHICLES = 17400;

        //Accounts for Current Liabilities
        public static readonly int NOTE_PAYABLE = 20100;
        public static readonly int ACCOUNT_PAYABLE = 20200;
        public static readonly int WAPGES_PAYABLE = 20300;
        public static readonly int INTEREST_PAYABLE = 20400;
        public static readonly int UNEARNED_REVENUES = 20500;
        public static readonly int MORTGAGE_LOAN_PAYABLE = 20500;

        //Accounts for Equity
        public static readonly int OWNER_CAPITAL = 30100;
        public static readonly int OWNER_DRAWINGS = 30200;

        //Accounts for Income
        public static readonly int SALES_FOOD = 40100;
        public static readonly int CASH_DISCOUNT = 40200;
        public static readonly int SERVICE_CHARGES = 40300;
        public static readonly int CONTRA_INCOME = 40400;

        //Accounts for Expenses
        public static readonly int SALARIES_EXPENSE = 50100;
        public static readonly int WAGES_EXPENSE = 50200;
        public static readonly int SUPPLIES_EXPENSE = 50300;
        public static readonly int RENT_EXPENSE = 50400;
        public static readonly int UTILITIES_EXPENSE = 50500;
        public static readonly int TELEPHONE_EXPENSE = 50600;
        public static readonly int ADVERTISING_EXPENSE = 50700;
        public static readonly int DEPRECECIATION_EXPENSE = 50800;
        public static readonly int OTHER_EXPENSE = 50900;

        //Accounts for Gains/Losses
        public static readonly int GAIN_ON_ASSETS = 60100;
        public static readonly int LOSS_ON_ASSETS = 60200;
    }

    [ComplexType]
    public class EFPhone
    {
        public string Mobile { get; set; }
        public string Office { get; set; }
        public string Home { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [ComplexType]
    public class EFAddress
    {
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class EFCategory
    {
        public EFCategory()
        {
            this.Items = new List<EFItem>();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public List<EFItem> Items { get; set; }
    }

    public enum ItemType
    {
        Supply, //Organization buy supplies 
        Inventory //  product that is sold Organization 
    }
    /// <summary>
    /// 
    /// </summary>
    public class EFItem
    {
        public EFItem()
        {
            this.MenuOrderItems = new List<EFOrderEntry>();
            this.OptionItems = new List<EFOptionItem>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        //[Index(), Required]
        public int Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }
        [Required]
        public double PreparationDueTime { get; set; }
        [Required]
        public double PreparationOverDueTime { get; set; }
        [Required]
        public bool IsAvailable { get; set; }
        [Required]
        public EFItemUnit Unit { get; set; }
        [Required]
        public EFCategory Category { get; set; }
        //Default user who will cook/prepare it
        //public ApplicationUser Cook { get; set; }
        public List<EFOrderEntry> MenuOrderItems { get; set; }
        public List<EFOptionItem> OptionItems { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class EFOrder
    {
        public EFOrder()
        {
            this.Entries = new List<EFOrderEntry>();
            //this.ExtendedProperties = new ExtendedProperties();
        }

        public enum OrderStatus
        {
            Open = 1,
            Served = 2,
            Cancelled = 3,
            Closed = 4
        }

        [Key]
        public int Id { get; set; }
        public System.DateTimeOffset CreatedOn { get; set; }
        public System.DateTimeOffset UpdatedOn { get; set; }
        public System.DateTimeOffset ClosedOn { get; set; }
        [DefaultValue(false)]
        public bool IsByCredit { get; set; }
        public OrderStatus Status { get; set; }
        [ForeignKey("ServingTable"), Column("ServingTable_Id")]
        public int? ServingTableId { get; set; }
        public EFServingTable ServingTable { get; set; }

        [ForeignKey("Customer"), Column("Customer_Id")]
        public string CustomerId { get; set; }
        //[Role(SystemUserRoles.Customer)]
        //public ApplicationUser Customer { get; set; }
        //[ForeignKey("OrderTakenBy"), Column("OrderTakenBy_Id")]
        //public string OrderTakenById { get; set; }
        //[Role(SystemUserRoles.OrderStaff)]
        //public ApplicationUser OrderTakenBy { get; set; }

        public List<EFOrderEntry> Entries { get; set; }

        [ForeignKey("PaidBy"), Column("PaidBy_Id")]
        public int? PaidById { get; set; }
        public EFVoucher PaidBy { get; set; }
        public decimal Discount { get; set; } //Exact amount
        public decimal ServiceCharges { get; set; } //Exact amount
        public decimal CashPaid { get; set; } //Exact amount
        public decimal PayableAmount { get; set; }
        //public ExtendedProperties ExtendedProperties { get; set; }
    }

    public class EFOrderPhoto
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public EFOrder Order { get; set; }
        [Required]
        public byte[] Photo { get; set; }
        [Required]
        public DateTime TakenOn { get; set; }
    }

    /// <summary>
    /// OptionSet refer to a set of option from which one can select
    /// </summary>
    public class EFOptionItemSet
    {
        //[Key, Column(Order = 0), ForeignKey("Item")]
        public int ItemId { get; set; }
        public EFItem Item { get; set; }
        //[Key, Column(Order = 1), ForeignKey("OptionItem")]
        public int OptionItemId { get; set; }
        public EFOptionItem OptionItem { get; set; }
    }
    /// <summary>
    /// Represet one of the option in optionset
    /// </summary>
    public class EFOptionItem
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        //[Index("UniqueName", 1, IsUnique = true)]
        public string Name { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EFOrderEntryOption
    {
        //[Key, Column(Order = 0), ForeignKey("OrderEntry")]
        public int OrderEntryId { get; set; }
        public EFOrderEntry OrderEntry { get; set; }
        //[Key, Column(Order = 1), ForeignKey("SelectedOption")]
        public int OptionId { get; set; }
        public EFOptionItem SelectedOption { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class EFOrderEntry
    {
        public enum Priority : int
        {
            AnyTime = 1,
            Urgent = 2
        }

        public enum State : int
        {
            InQueue = 1,
            Started = 2,
            Ready = 3,
            Served = 4,
            Cancelled = 5,
        }

        public EFOrderEntry()
        {
            this.OrderEntryOptions = new List<EFOrderEntryOption>();
            //this.ExtendedProperties = new ExtendedProperties();
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public decimal Quantity { get; set; }
        [Required]
        public decimal SalePrice { get; set; }
        [ForeignKey("Order"), Required]
        public int OrderId { get; set; }
        public EFOrder Order { get; set; }
        [Required]
        public EFItem Item { get; set; }
        [Required]
        public bool TakeAway { get; set; }
        public List<EFOrderEntryOption> OrderEntryOptions { get; set; }
        [Required]
        public System.DateTimeOffset CreatedOn { get; set; }
        public System.DateTime? CancelledOn { get; set; }
        [Required]
        public State CurrentState { get; set; }
        [Required]
        public Priority CurrentPriority { get; set; }
        //[Role(SystemUserRoles.KitchenAdmins)]
        //public ApplicationUser CancelledBy { get; set; }
        //public ExtendedProperties ExtendedProperties { get; set; }
        public void Init(EFItem item)
        {
            Item = item;
            CreatedOn = DateTime.Now;
            CurrentState = State.InQueue;
        }
    }

    /*[ComplexType]
    public class ExtendedProperties
    {
        public string JSonBuffer { get; set; }
        [NotMapped]
        private Dictionary<string, object> m_cache = null;
        private void _Save()
        {
            JSonBuffer = Newtonsoft.Json.JsonConvert.SerializeObject(m_cache);
        }
        private void _Load()
        {
            if (m_cache == null)
            {
                if (!string.IsNullOrWhiteSpace(JSonBuffer))
                {
                    m_cache = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(JSonBuffer);
                }
                else
                    m_cache = new Dictionary<string, object>();
            }
        }
        public bool TryGetValue<T>(string property, ref T value)
        {
            _Load();
            value = default(T);
            if (!m_cache.ContainsKey(property))
                return false;

            var v = m_cache[property];
            if (v != null)
            {
                value = (T)v;
            }

            return true;
        }
        public T GetValue<T>(string property, T defaultValue)
        {
            _Load();
            if (!m_cache.ContainsKey(property))
                return defaultValue;

            return (T)m_cache[property];
        }
        public void SetValue<T>(string property, T value)
        {
            _Load();
            m_cache[property] = value;
            _Save();
        }
        public void Remove(string property)
        {
            _Load();
            if (m_cache.ContainsKey(property))
            {
                m_cache.Remove(property);
            }
        }
        public IEnumerable<string> Keys
        {
            get
            {
                _Load();
                return m_cache.Keys;
            }
        }
        public IEnumerable<object> Values
        {
            get
            {
                _Load();
                return m_cache.Values;
            }
        }
    }*/

    /// <summary>
    /// 
    /// </summary>
    public class EFServingTable
    {
        public enum ReservationStatus
        {
            Open = 0,
            Reserved = 1,
            InUse = 2
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public int NoOfSeats { get; set; }
        [Required]
        public ReservationStatus Status { get; set; }
        [Required]
        public string Label { get; set; }
        [Required]
        public EFSection Section { get; set; }
        //public List<EFOrder> MenuOrders { get; set; }
    }

    /// <summary>
    /// Serving section
    /// </summary>
    public class EFSection
    {
        public EFSection()
        {
            this.Tables = new List<EFServingTable>();
        }

        [Key]
        public int Id { get; set; }
        [Required]
        public string Label { get; set; }
        public List<EFServingTable> Tables { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EFItemUnit
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ShortName { get; set; }
        [Required]
        public int DivisableBy { get; set; }
    }

    /*
     * Model classes for Accounts
     */

    public class EFVoucher
    {
        public EFVoucher()
        {
            Entries = new List<EFVoucherEntry>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public DateTime CreatedOn { get; set; }
        //[Required]
        //public ApplicationUser CreatedBy { get; set; }
        [Required]
        public string Description { get; set; }
        public List<EFVoucherEntry> Entries { get; set; }
        public void Validate()
        {
            if (Entries.Count == 0)
                throw new Exception("Voucher must have at least two entries");

            decimal credit = 0, debit = 0;

            foreach (var entry in Entries)
            {
                if (entry.Amount == 0)
                    throw new Exception("Entry in voucher cannot have zero amount");

                if (entry.Amount < 0)
                    throw new Exception("Entry in voucher cannot have negative amount");

                if (entry.EntryType == EntryType.Credit)
                    credit += entry.Amount;
                else if (entry.EntryType == EntryType.Debit)
                    debit += entry.Amount;
                else
                    throw new Exception("Entry in voucher must have either credit or debit type.");

                if (entry.Account == null)
                    throw new Exception("Entry must have a valid account");
            }

            if ((credit - debit) != 0)
                throw new Exception("Voucher must have Sum(Credit) == Sum(Debit).");

            if (string.IsNullOrWhiteSpace(Description))
                throw new Exception("Description must be provided");

            //if (CreatedBy == null)
                //throw new Exception("User must be provided who created the voucher");
        }
    }

    public class EFHead
    {
        public EFHead()
        {
            SubHeads = new List<EFSubHead>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public EntryType IncreasedBy { get; set; }
        [Required]

        public string Description { get; set; }
        public List<EFSubHead> SubHeads { get; set; }
    }

    public class EFSubHead
    {
        public EFSubHead()
        {
            Accounts = new List<EFAccount>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public EFHead Head { get; set; }
        public List<EFAccount> Accounts { get; set; }
    }

    public class EFAccount
    {
        public EFAccount()
        {
            this.Entries = new List<EFVoucherEntry>();
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [DefaultValue(false)]
        public bool IsBroughtForward { get; set; }
        [Required]
        public EFSubHead SubHead { get; set; }
        //public ApplicationUser LinkToUser { get; set; }
        public bool Freeze { get; set; }
        public List<EFVoucherEntry> Entries { get; set; }
    }

    public enum EntryType
    {
        Credit = 0,
        Debit = 1
    }
    public class EFVoucherEntry
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public EFAccount Account { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]
        public EntryType EntryType { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public EFVoucher Voucher { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal? ClosingBalance { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal? OpeningBalance { get; set; }

    }

    /*
     * End of Model classes for Accounts
     */

    /*
     * Model classes for Inventory
     * 
     */

    public class EFInvoice
    {
        EFInvoice()
        {
            Entries = new List<EFInvoiceEntry>();
        }
        [Key]
        public int Id { get; set; }
        //public ApplicationUser SuppliedBy { get; set; }
        //public ApplicationUser CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public EFVoucher PaidBy { get; set; }
        public List<EFInvoiceEntry> Entries { get; set; }
    }

    public enum InvoiceEntryType
    {
        In,
        Out
    }
    public class EFInvoiceEntry
    {
        [Key]
        public int Id { get; set; }
        EFItem Item { get; set; }
        [Required]
        public EFInvoice Invoice { get; set; }
        [Required]
        public decimal Quantity { get; set; }
        [Required]
        InvoiceEntryType EntryType { get; set; }
    }

    public class EFSettings
    {
        [Key]
        public int Id { get; set; }
        public decimal ServiceChargesPercent { get; set; }
        public decimal DiscountPercent { get; set; }
        public string CompanyName { get; set; }
        public string Logo { get; set; }
    }
}
