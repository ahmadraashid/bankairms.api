﻿using BankaiRMS.DAL.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankaiRMS.APIs
{
    public class DbInitializer
    {
        public RMSDbContext context;

        public DbInitializer()
        {

        }

        public void SetDbContext(RMSDbContext ctx)
        {
            context = ctx;
        }

        public void Seed()
        {
            // Run Migrations
            context.Database.Migrate();

            if (!context.Categories.Any())
            {
                // Seeding the Database
                // ...
                //context.SaveChanges();
            }
        }
    }
}
