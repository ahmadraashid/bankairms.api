﻿using BankaiRMS.Models.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace BankaiRMS.DAL.EF
{
    public class RMSDbContext : DbContext
    {
        public RMSDbContext()
        {
        }

        public RMSDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            /*var builder = new ConfigurationBuilder()
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            string connectionString = configuration.GetSection("Logging:ConnectionStrings:DefaultConnection").Value;
            optionsBuilder.UseSqlServer(connectionString);*/
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EFOptionItemSet>()
                .HasKey(os => new { os.ItemId, os.OptionItemId });

            modelBuilder.Entity<EFOrderEntryOption>()
                .HasKey(eo => new { eo.OptionId, eo.OrderEntryId });

            modelBuilder.Entity<EFItem>()
                .HasIndex(i => i.Code)
                .IsUnique();
        }


        public DbSet<EFCategory> Categories { get; set; }
        public DbSet<EFItem> Items { get; set; }
        public DbSet<EFOrder> Orders { get; set; }
        public DbSet<EFOrderEntry> OrderEntries { get; set; }
        public DbSet<EFOrderPhoto> OrderPhotos { get; set; }
        public DbSet<EFServingTable> ServingTables { get; set; }
        public DbSet<EFSettings> Settings { get; set; }
        public DbSet<EFItemUnit> ItemUnits { get; set; }
        public DbSet<EFSection> Sections { get; set; }
        public DbSet<EFInvoice> Invoices { get; set; }
        public DbSet<EFInvoiceEntry> InvoiceEntries { get; set; }

        //Accounts
        public DbSet<EFAccount> Accounts { get; set; }
        public DbSet<EFHead> Heads { get; set; }
        public DbSet<EFSubHead> SubHeads { get; set; }
        public DbSet<EFVoucher> Vouchers { get; set; }
        public DbSet<EFVoucherEntry> VoucherEntires { get; set; }
        public DbSet<EFOptionItem> OptionItems { get; set; }
        public DbSet<EFOptionItemSet> OptionItemSets { get; set; }
        public DbSet<EFOrderEntryOption> OrderEntryOptions { get; set; }
        //Overridden SaveChanges to catch full exception details about
        //EntityValidation Exceptions instead of attaching debugger everytime
        public override int SaveChanges()
        {
            try
            {
                var entities = from e in ChangeTracker.Entries()
                               where e.State == EntityState.Added
                                   || e.State == EntityState.Modified
                               select e.Entity;
                foreach (var entity in entities)
                {
                    var validationContext = new ValidationContext(entity);
                    Validator.ValidateObject(entity, validationContext);
                }

                return base.SaveChanges();
            }
            catch(DbUpdateException ex)
            {
                throw new DbUpdateException(ex.Message, ex);
            }
            /*catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);
                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (DbUpdateException ex)
            {
                var decodedErrors = TryDecodeDbUpdateException(ex);
                if (decodedErrors == null)
                    throw; //it isn't something we understand so rethrow

                throw new DbUpdateException(decodedErrors);
            }*/
        }

        /*string TryDecodeDbUpdateException(DbUpdateException ex)
        {
            if (!(ex.InnerException is System.Data.Entity.Core.UpdateException) ||

                !(ex.InnerException.InnerException is System.Data.SqlClient.SqlException))
                return null;

            var sqlException = (System.Data.SqlClient.SqlException)ex.InnerException.InnerException;
            //var result = new List<ValidationResult>();
            string errorText = "";
            for (int i = 0; i < sqlException.Errors.Count; i++)
            {
                var errorNum = sqlException.Errors[i].Number;

                bool gotValue = _sqlErrorTextDict.TryGetValue(errorNum, out errorText);
                //result.Add(new ValidationResult(errorText));
            }
            //return result.Any() ? result : null;
            return errorText;
        }*/

        private static readonly Dictionary<int, string> _sqlErrorTextDict = new Dictionary<int, string>
            {
                {547, "This operation failed because another data entry uses this entry."},
                {2601,"One of the properties is marked as Unique index and there is already an entry with that value."}
            };

    }
}
